package com.helpswapper.offer;

import org.springframework.data.annotation.Id;

import java.util.UUID;

public class Offer {
    @Id
    private String id = UUID.randomUUID().toString();
    private String title = "";
    private String description = "";
    private String image = "";
    private OfferCategory category = OfferCategory.OTHER;
    private OfferRefund refund = OfferRefund.OTHER;
    private String contactData = "";



    public Offer(){
        //empty constructor required by framework
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public OfferCategory getCategory() {
        return category;
    }

    public OfferRefund getRefund() {
        return refund;
    }

    public String getContactData() {
        return contactData;
    }
}
