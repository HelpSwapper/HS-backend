package com.helpswapper.offer;

public enum OfferCategory {
    GARDENING,
    BABYSITTING,
    RENOVATION,
    COOKING,
    SHOPPING,
    TRANSPORTATION,
    CRAFTSMANSHIP,
    CLEANING,
    OTHER,
    FOR_WOMAN,
    FOR_MEN,
    LESSONS,
    SKILLS_EXCHANGE,
    ANIMALS_HELP;
}
