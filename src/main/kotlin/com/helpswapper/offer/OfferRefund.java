package com.helpswapper.offer;

public enum OfferRefund {
    FOOD,
    CLOTHES,
    ACCOMMODATION,
    DEVICES,
    FURNITURES,
    TOOLS,
    BABY_ACCESSORIES,
    TICKETS,
    COMPANIONSHIP,
    DRUGS,
    TOYS,
    OTHER,
    SKILLS_EXCHANGE,
    BOOKS,
    ELECTRICAL_APPLIANCES,
    HOME_ACCESSORIES;
}
