package com.helpswapper.offer;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//save data to mongodb

@Repository
public interface OfferRepository extends MongoRepository<Offer, String>{
    //repository already has some predefined queries (findById)
    List<Offer> findAllByCategory(OfferCategory category);

    List<Offer> findAllByRefund(String refund);
}
