package com.helpswapper.offer;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

//all logic goes here, e.x validation

@Service
public class OfferService {
    private OfferRepository repository;

    public OfferService(OfferRepository repository){
        this.repository = repository;
    }

    public List<Offer> retrieveAll() {
        return repository.findAll();
    }

    public List<Offer> retrieveAllByCategory(String category){
        return repository.findAllByCategory(OfferCategory.valueOf(category));
    }

    public Offer create(Offer offer) {
        return repository.insert(offer);
    }

    public Offer retrieveById(String id) {
        return repository.findOne(id);
    }

    public List<Offer> retrieveAllByRefundCategory(String category) {
        return repository.findAllByRefund(category);
    }

    public void delete(String id){
        repository.delete(id);
    }

    public void deleteAll(){
        repository.deleteAll();
    }
}
