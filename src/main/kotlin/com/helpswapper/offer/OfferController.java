package com.helpswapper.offer;

import org.springframework.web.bind.annotation.*;

import java.util.List;

// retrieve http request, push it to service, return http response

@RestController
@RequestMapping("/api/offers")
@CrossOrigin(origins = "*")

public class OfferController {

    private OfferService service;

    public OfferController(OfferService service) {
        this.service = service;
    }

    @GetMapping
    public List<Offer> retrieveAll() {
        return service.retrieveAll();
    }

    @GetMapping("/{id}")
    public Offer getById(@PathVariable("id") String id) {
        return service.retrieveById(id);
    }


    @GetMapping("/categories")
    public OfferCategory[] getCategories() {
        return OfferCategory.values();
    }

    @GetMapping("refunds")
    public OfferRefund[] getRefund(){
        return OfferRefund.values();
    }

    @GetMapping("/by/{category}")
    public List<Offer> findByCategory(@PathVariable("category") String category) {
        return service.retrieveAllByCategory(category);
    }

    @GetMapping("/byRefund/{category}")
    public List<Offer> findByRefundCategory(@PathVariable("category") String category) {
        return service.retrieveAllByRefundCategory(category);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id) {
        service.delete(id);
    }

    @DeleteMapping("/deleteAll")
    public void deleteAll(){
        service.deleteAll();
    }

    @PostMapping
    public Offer createOffer(@RequestBody Offer offer) {
        return service.create(offer);
    }

}
